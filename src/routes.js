import React from 'react'
import { Route, Redirect } from 'react-router'
import { UserAuthWrapper } from 'redux-auth-wrapper'

import App from './components/App'
import Events from './components/Events/Events'
import EventDetail from './components/Events/EventDetail'
import CreateEvent from './components/Events/CreateEvent'
import Login from './components/Auth/Login'
import Signup from './components/Auth/Signup'
import Profile from './components/Auth/Profile'
import { NotFound } from './components/Common/NotFound'

const UserIsAuthenticated = UserAuthWrapper({
  authSelector: state => state.user,
  wrapperDisplayName: 'UserIsAuthenticated'
})

export default (
  <div>
    <Redirect from="/" to="/events" />
    <Route path="/" component={App}>
      <Route path="events" component={UserIsAuthenticated(Events)} />
      <Route path="event/:id" component={UserIsAuthenticated(EventDetail)} />
      <Route path="create" component={UserIsAuthenticated(CreateEvent)} />
      <Route path="profile" component={UserIsAuthenticated(Profile)} />
      <Route path="login" component={Login} />
      <Route path="signup" component={Signup} />
      <Route path="*" component={NotFound} />
    </Route>
  </div>
)