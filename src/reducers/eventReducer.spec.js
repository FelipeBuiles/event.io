import * as ActionTypes from '../constants/actionTypes'
import reducer from './eventReducer'

describe('Reducers::Event', () => {
  const getInitialState = () => {
    return []
  }

  const getAppState = () => {
    return [
      {
        id: 'a1',
        title: 'dummy event'
      },
      {
        id: 'b2',
        title: 'another dummy event'
      }
    ]
  }

  it('should set initial state by default', () => {
    const action = { type: 'unknown' }
    const expected = getInitialState()

    expect(reducer(undefined, action)).toEqual(expected)
  })

  it('should handle LOAD_EVENTS_SUCCESS', () => {
    const action = { type: ActionTypes.LOAD_EVENTS_SUCCESS, events: getAppState() }
    const expected = getAppState()

    expect(reducer(getInitialState(), action)).toEqual(getAppState())
  })

  it('should add a single event with LOAD_EVENT_SUCCESS', () => {
    let action = { type: 'unknown' }
    let actual = reducer(getInitialState(), action).length
    let expected = 0

    expect(actual).toEqual(expected)

    action = { type: 'LOAD_EVENT_SUCCESS', event: {id: 'a1', title: 'dummy event'}}
    actual = reducer(getInitialState(), action).length
    expected = 1

    expect(actual).toEqual(expected)
  })

  it('should handle event creation with CREATE_EVENT_SUCCESS', () => {
    const action = { type: ActionTypes.CREATE_EVENT_SUCCESS, event: {id: 'c3'}}
    const actual = reducer(getAppState(), action).length
    const expected = 3

    expect(actual).toEqual(expected)
  })

  it('should update an existing event with UPDATE_EVENT_SUCCESS', () => {
    const action = { type: ActionTypes.UPDATE_EVENT_SUCCESS, event: {id: 'a1', title: 'new title'}}
    const actual = reducer(getAppState(), action)[0].title
    const expected = 'new title'

    expect(actual).toEqual(expected)
  })

  it('should delete an event with DELETE_EVENT_SUCCESS', () => {
    const action = { type: ActionTypes.DELETE_EVENT_SUCCESS, id: 'a1'}
    const actual = reducer(getAppState(), action).length
    const expected = 1

    expect(actual).toEqual(expected)
  })
})