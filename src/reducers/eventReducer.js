import * as types from '../constants/actionTypes'
import initialState from './initialState'

const compare = (a, b) => (a.id > b.id) ? 1 : -1;

export default function events(state = initialState.events, action) {
  if (action.type == types.LOAD_EVENTS_SUCCESS) {
    return [...action.events].sort(compare)
  }
  if (action.type == types.LOAD_EVENT_SUCCESS
      || action.type == types.SET_ATTENDANCE_SUCCESS
      || action.type == types.UPDATE_EVENT_SUCCESS) {
    return [
      ...state.filter(event => event.id !== action.event.id),
      action.event
    ].sort(compare)
  }
  if (action.type == types.CREATE_EVENT_SUCCESS) {
    return [
      ...state,
      action.event
    ].sort(compare)
  }
  if (action.type == types.DELETE_EVENT_SUCCESS) {
    return [
      ...state.filter(event => event.id !== action.id)
    ]
  }
  return state
}