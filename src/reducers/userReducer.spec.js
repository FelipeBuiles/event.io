import * as ActionTypes from '../constants/actionTypes'
import reducer from './userReducer'

describe('Reducers::User', () => {
  const getInitialState = () => {
    return {}
  }

  const getAppState = () => {
    return {
      id: 'a1',
      firstName: 'John',
      lastName: 'Doe'
    }
  }

  it('should set initial state by default', () => {
    const action = { type: 'unknown' }
    const expected = getInitialState()

    expect(reducer(undefined, action)).toEqual(expected)
  })

  it('should handle LOGIN_SUCCESS', () => {
    const action = { type: ActionTypes.LOGIN_SUCCESS, user: getAppState()}
    const expected = getAppState()
    const actual = reducer(getInitialState(), action)

    expect(expected).toEqual(actual)
  })

  it('should handle LOGIN_FAILURE', () => {
    const action = { type: ActionTypes.LOGIN_FAILURE }
    const expected = { bad_login:true }
    const actual = reducer(getInitialState(), action)

    expect(expected).toEqual(actual)
  })

  it('should handle SIGNUP_SUCCESS', () => {
    const action = { type: ActionTypes.SIGNUP_SUCCESS, user: getAppState()}
    const expected = getAppState()
    const actual = reducer(getInitialState(), action)

    expect(expected).toEqual(actual)
  })

  it('should handle SIGNUP_FAILURE', () => {
    const action = { type: ActionTypes.SIGNUP_FAILURE }
    const expected = { bad_signup:true }
    const actual = reducer(getInitialState(), action)

    expect(expected).toEqual(actual)
  })

  it('should handle LOGOUT', () => {
    const action = { type: ActionTypes.LOGOUT }
    const expected = {}
    const actual = reducer(getAppState(), action)

    expect(expected).toEqual(actual)
  })

  it('should handle TRIGGER_REAUTH', () => {
    const action = { type: ActionTypes.TRIGGER_REAUTH }
    const expected = Object.assign({}, getAppState(), { timedOut: true })
    const actual = reducer(getAppState(), action)

    expect(expected).toEqual(actual)
  })
})