 import { combineReducers } from 'redux'
 import { routerReducer } from 'react-router-redux'
 import { reducer as formReducer } from 'redux-form'
 import ajaxCallsInProgress from './ajaxStatusReducer'
 import user from './userReducer'
 import events from './eventReducer'

const rootReducer = combineReducers({
  ajaxCallsInProgress,
  user,
  events,
  form: formReducer,
  routing: routerReducer
})

export default rootReducer
