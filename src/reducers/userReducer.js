import * as types from '../constants/actionTypes'
import initialState from './initialState'

export default function user (state = initialState.user, {type, user})  {
  if (type == types.LOGIN_SUCCESS) {
    return user
  }
  if (type == types.LOGIN_FAILURE) {
    return { bad_login: true } 
  }
  if (type == types.SIGNUP_SUCCESS) {
    return user
  }
  if (type == types.SIGNUP_FAILURE) {
    return { bad_signup: true } 
  }
  if (type == types.LOGOUT) {
    return {}
  }
  if (type == types.TRIGGER_REAUTH) {
    return {
      ...state,
      timedOut: true
    }
  }
  return state
}