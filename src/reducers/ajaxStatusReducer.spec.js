import * as ActionTypes from '../constants/actionTypes'
import reducer from './ajaxStatusReducer'

describe('Reducers::AjaxStatus', () => {
  const getInitialState = () => {
    return 0
  }

  const getAppState = () => {
    return 3
  }

  it('should set initial state by default', () => {
    const action = { type: 'unknown' }
    const expected = getInitialState()

    expect(reducer(undefined, action)).toEqual(expected)
  })

  it('should handle BEGIN_AJAX_CALL', () => {
    const action = { type: ActionTypes.BEGIN_AJAX_CALL }
    const expected = 4

    expect(reducer(getAppState(), action)).toEqual(expected)
  })

  it('should handle AJAX_CALL_ERROR', () => {
    const action = { type: ActionTypes.AJAX_CALL_ERROR }
    const expected = 2

    expect(reducer(getAppState(), action)).toEqual(expected)
  })
})