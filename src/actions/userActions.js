import * as types from '../constants/actionTypes'
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions'
import * as api from '../api/user'

export const loginSuccess = user => {
  return { type: types.LOGIN_SUCCESS, user }
}

export const loginFailure = () => {
  return { type: types.LOGIN_FAILURE }
}

export const signupSuccess = user => {
  return { type: types.SIGNUP_SUCCESS, user }
}

export const signupFailure = () => {
  return { type: types.SIGNUP_FAILURE }
}

export function logout() {
  return { type: types.LOGOUT }
}

export function triggerReauth() {
  return { type: types.TRIGGER_REAUTH }
} 

export function login(email, password) {
  return dispatch => {
    dispatch(beginAjaxCall())
    api.login(email, password)
      .then(res => {
        if (res.headers.has('authorization')) {
          window.sessionStorage.setItem('authorization', res.headers.get('authorization'))
          return res.json()
        }
      })
      .then(user => {
        if (user) {
          dispatch(loginSuccess(user))
        } else {
          dispatch(loginFailure())
        }
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export function signup({firstName, lastName, email, password}) {
  return dispatch => {
    dispatch(beginAjaxCall())
    api.signup(firstName, lastName, email, password)
      .then(res => {
        if (res.status == 201) {
          if (res.headers.has('authorization')) {
            window.sessionStorage.setItem('authorization', res.headers.get('authorization'))
            return res.json()
          }
        }
      })
      .then(user => {
        if (user) {
          dispatch(signupSuccess(user))
        } else {
          dispatch(signupFailure())
        }
      })
      .catch(() => {
        dispatch(ajaxCallError())
      })
  }
}