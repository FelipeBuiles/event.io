import * as types from '../constants/actionTypes'
import { triggerReauth } from './userActions'   
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions'
import * as eventApi from '../api/event'

export const loadEventsSuccess = events => {
  return { type: types.LOAD_EVENTS_SUCCESS, events }
}

export const loadEventSuccess = event => {
  return { type: types.LOAD_EVENT_SUCCESS, event }
}

export const updateEventSuccess = event => {
  return { type: types.UPDATE_EVENT_SUCCESS, event }
}

export const createEventSuccess = event => {
  return { type: types.CREATE_EVENT_SUCCESS, event }
}

export const deleteEventSuccess = id => {
  return { type: types.DELETE_EVENT_SUCCESS, id }
}

const handleCommonRes = (res, dispatch) => {
  if (res.status == 200 || res.status == 201) {
    return res.json()
  } else {
    dispatch(triggerReauth())
  }
}

export const loadEvents = () => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.list()
      .then(res => {
        return handleCommonRes(res, dispatch)
      })
      .then(events => {
        dispatch(loadEventsSuccess(events))
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export const loadEvent = id => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.getSpecific(id)
      .then(res => {
        return handleCommonRes(res, dispatch)
      })
      .then(event => {
        dispatch(loadEventSuccess(event))
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export const joinEvent = id => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.attend(id)
      .then(res => {
        return handleCommonRes(res, dispatch)
      })
      .then(event => {
        dispatch(setAttendanceSuccess(event))
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export const leaveEvent = id => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.unattend(id)
      .then(res => {
        return handleCommonRes(res, dispatch)
      })
      .then(event => {
        dispatch(setAttendanceSuccess(event))
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export const setAttendanceSuccess = event => {
  return { type: types.SET_ATTENDANCE_SUCCESS, event }
}

export const updateEvent = (id, event) => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.update(id, event)
      .then(res => {
        return handleCommonRes(res, dispatch)
      })
      .then(event => {
        dispatch(updateEventSuccess(event))
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export const createEvent = event => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.create(event)
      .then(res => {
        return handleCommonRes(res, dispatch)
      })
      .then(event => {
        dispatch(createEventSuccess(event))
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}

export const deleteEvent = id => {
  return dispatch => {
    dispatch(beginAjaxCall())
    eventApi.remove(id)
      .then(res => {
        if(res.status == 204) {
          dispatch(deleteEventSuccess(id))
        } else {
          dispatch(triggerReauth())
        }
      })
      .catch(error => {
        dispatch(ajaxCallError(error))
      })
  }
}