import React, { Component } from 'react'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import { TextField } from 'redux-form-material-ui'
import PropTypes from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'

const btnStyle = {height: 57, width: 240}

class SignupForm extends Component {
  static propTypes = {
    password: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
  }

  required = value => (value == null ? 'Required' : undefined)
  email = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email'
    : undefined)
  repeatPassword = value => {
    if (value && this.props.password) {
      if(value !== this.props.password) {
        return 'Passwords don\'t match'
      }
    }
    return undefined
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
        <div>
          <Field type="text"
            name="firstName"
            floatingLabelText="First name"
            fullWidth={true}
            component={TextField}
            validate={this.required}
            />
        </div>
        <div>
          <Field type="text"
            name="lastName"
            floatingLabelText="Last name"
            fullWidth={true}
            component={TextField} 
            validate={this.required}
            />
        </div>
        <div>
          <Field type="email"
            name="email"
            floatingLabelText="Email"
            fullWidth={true}
            component={TextField} 
            validate={[this.required, this.email]}
            />
        </div>
        <div>
          <Field type="password"
            name="password"
            floatingLabelText="Password"
            fullWidth={true}
            component={TextField}
            validate={this.required}
            />
        </div>
        <div>
          <Field type="password"
            name="password-repeat"
            floatingLabelText="Repeat password"
            fullWidth={true}
            component={TextField}
            validate={[this.required, this.repeatPassword]}
            />
        </div>
        <RaisedButton style={btnStyle}
          className="submit-button"
          label="Sign up"
          primary={true}
          type="submit" />
      </form>
    )
  }
}

const selector = formValueSelector('signup')

export default connect(state => ({
  password: selector(state, 'password')
}))(reduxForm({
  form: 'signup'
})(SignupForm))