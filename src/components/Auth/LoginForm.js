import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import { TextField } from 'redux-form-material-ui' 
import PropTypes from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'
import SvgIcon from 'material-ui/SvgIcon'
import Visibility from 'material-ui/svg-icons/action/visibility' 
import VisibilityOff from 'material-ui/svg-icons/action/visibility-off'

class LoginForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
  }

  state = {
    hiddenPassword: true
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.badLogin) {
      this.underlineStyle = {borderColor: '#FF4081'}
    }
  }
 
  btnStyle = {height: 57, width: 240}
  underlineStyle = {boderColor: '#1BE38B'}

  toggleVisibility = () => {
    this.setState({
      hiddenPassword: !this.state.hiddenPassword
    })
  }

  renderToggleButton = () => {
    if (this.state.hiddenPassword) {
      return (
        <SvgIcon className="visibility-toggle"
          onTouchTap={this.toggleVisibility}>
          <Visibility />
        </SvgIcon>
      )   
    } else {
      return (
        <SvgIcon className="visibility-toggle"
          onTouchTap={this.toggleVisibility}>
          <VisibilityOff />
        </SvgIcon>
      )
    }
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
          <div>
            <Field type="email"
              name="email"
              floatingLabelText="Email"
              fullWidth={true}
              component={TextField}
              underlineFocusStyle={this.underlineStyle} />
          </div>
          <div className="password-field">
            <Field type={this.state.hiddenPassword ? 'password' : 'text'}
              name="password"
              floatingLabelText="Password"
              fullWidth={true}
              component={TextField}
              underlineFocusStyle={this.underlineStyle} />
            {this.renderToggleButton()}
          </div>
          <Link className="forgot" to="/404">Forgot password?</Link>
          <RaisedButton style={this.btnStyle}
            label="Sign in"
            className="submit-button"
            primary={true}
            type="submit" />
        </form>
    )
  }
}

export default reduxForm({
  form: 'login'
})(LoginForm)