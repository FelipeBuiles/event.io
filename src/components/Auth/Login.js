import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { login } from '../../actions/userActions'
import LoginForm from './LoginForm'
import SidePanel from '../Common/SidePanel'
import './auth.scss'

class Login extends Component {
  static propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    redirect: PropTypes.string,
    user: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  componentWillMount() {
    const { isAuthenticated, redirect } = this.props
    if (isAuthenticated) {
      this.context.router.push(redirect)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { isAuthenticated, redirect } = nextProps
    const { isAuthenticated: wasAuthenticated } = this.props

    if (!wasAuthenticated && isAuthenticated) {
      this.context.router.push(redirect)
    }
  }

  handleLogin = values => {
    this.props.login(values)
  }

  renderSubtitle = () => {
    if (this.props.user.bad_login) {
      return (
        <p className="error-text">
          Oops! That email and password combination is not valid.
        </p>
      )
    } else {
      return <p>Enter your details below.</p>
    }
  }

  render() {
    return (
      <div className="auth-container">
        <SidePanel />
        <img className="logo-m"
          alt="Eventio logo"
          src={require('../../assets/images/Logo.png')}
          />
        <div className="main-panel">
          <div className="center-panel">
            <div className="headline">
              <h1>Sign in to Eventio.</h1>
              {this.renderSubtitle()}
            </div>
            <LoginForm onSubmit={this.handleLogin} badLogin={this.props.user.bad_login}/>
            <Link className="sign-up" to="/signup">
              Don't have an account? <span className="emphasis">SIGN UP</span>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const select = state => {
  const isAuthenticated = !!state.user._id || false
  const redirect = '/'
  const user = state.user
  return {
    isAuthenticated,
    redirect,
    user
  }
}

export default connect(select, { login })(Login)