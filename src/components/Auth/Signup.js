import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { PropTypes } from 'prop-types'
import { signup } from '../../actions/userActions'
import SignupForm from './SignupForm'
import SidePanel from '../Common/SidePanel' 
import './auth.scss'

class Signup extends Component {
  static propTypes = {
    signup: PropTypes.func.isRequired,
    user: PropTypes.object
  }

  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  componentWillMount() {
    const isAuthenticated = !!this.props.user._id
    if (isAuthenticated) {
      this.context.router.push('/')
    }
  }

  componentWillReceiveProps(nextProps) {
    const isAuthenticated = !!nextProps.user._id
    const wasAuthenticated = !!this.props.user._id

    if (!wasAuthenticated && isAuthenticated) {
      this.context.router.push('/')
    }
  }

  handleSignup = values => {
    this.props.signup(values)
  }

  renderSubtitle = () => {
    if (this.props.user.bad_signup) {
      return (
        <p className="error-text">
          That user already exists.
        </p>
      )
    } else {
      return <p>Enter your details below.</p>
    }
  }

  render() {
    return (
      <div className="auth-container">
        <SidePanel />
        <img className="logo-m"
          alt="Eventio logo"
          src={require('../../assets/images/Logo.png')}
          />
        <div className="main-panel">
          <div className="center-panel">
            <div className="headline">
              <h1>Get started absolutely free.</h1>
              {this.renderSubtitle()}
            </div>
            <SignupForm onSubmit={this.handleSignup} />
            <Link className="sign-up" to="/login">
              Already have an account? <span className="emphasis">SIGN IN</span>
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, { signup })(Signup)