import React, { Component } from 'react'
import { connect } from 'react-redux'
import Avatar from 'react-avatar'
import PropTypes from 'prop-types'
import { Card, CardTitle } from 'material-ui/Card'
import EventCard from '../Events/EventCard'
import EventTile from '../Events/EventTile'
import Header from '../Common/Header'
import ViewControl from '../Events/ViewControl'
import { leaveEvent, loadEvents } from '../../actions/eventActions'
import './profile.scss'
import '../Events/event.scss'

class Profile extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  static propTypes = {
    events: PropTypes.array,
    user: PropTypes.object.isRequired,
    leaveEvent: PropTypes.func.isRequired,
    loadEvents: PropTypes.func.isRequired
  }

  state = {
    displayMode : 'grid'
  }

  componentDidMount() {
    if (!this.props.events.length) {
      this.props.loadEvents()
    }
  }

  setViewType = type => {
    this.setState({
      displayMode: type
    })
  }

  leaveEvent = id => {
    this.props.leaveEvent(id)
  }

  editEvent = id => {
    this.context.router.push({
      pathname: `/event/${id}`,
      state: {editing: true}
    })
  }

  renderEvents = () => {
    let filteredEvents = this.props.events.filter(e => {
      if (e.owner.id == this.props.user.id) return true
      if (e.attendees.find(a => a.id == this.props.user.id)) return true
      return false
    })
    return filteredEvents
      .sort()
      .map(e => {
        if (this.state.displayMode == 'grid') {
          return (
            <EventCard key={e._id} 
              event={e}
              edit={this.editEvent}
              user={this.props.user}
              leave={this.leaveEvent}/>
          )
        } else {
          return (
            <EventTile key={e._id}
              event={e}
              user={this.props.user}
              leave={this.leaveEvent} />
          )
        }
      })
  }

  render() {
    const username = `${this.props.user.firstName} ${this.props.user.lastName}`
    const email = this.props.user.email
    return (
      <div>
        <Header />
        <div className="profile-container">
          <Card className="profile-header">
            <Avatar name={username} size={120} className="avatar"/>
            <CardTitle className="profile-name" title={username} subtitle={email}/>
          </Card>
          <div className="my-events">
            <div className="top-controls">
              <h1>My Events</h1>
              <ViewControl currentActive={this.state.displayMode}
                setActive={this.setViewType} />
            </div>
            <div className={`list-area ${this.state.displayMode === 'list' ? 'list-mode' : ''}`}>
              {this.renderEvents()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    events: state.events
  }
}

export default connect(mapStateToProps, { leaveEvent, loadEvents })(Profile)