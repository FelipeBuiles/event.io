import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { logout } from '../actions/userActions'

const eventioTheme = getMuiTheme({
  fontFamily: 'Hind',
  palette: {
    textColor: '#323C46',
    primary1Color: '#22D486',
    accent1Color: '#FF4081',
    secondary1Color: '#D9DCE1',
    pickerHeaderColor: '#22D486'
  },
  spacing: {
    
  }
})

class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    logout: PropTypes.func.isRequired
  }

  state = {
    redirectModalOpen: false
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user.timedOut) {
      this.setState({
        redirectModalOpen: true
      })
    }
  }

  logout = () => {
    this.handleClose()
    this.props.logout()
  }

  handleClose = () => {
    this.setState({
      redirectModalOpen: false
    })
  }

  render() {
    const actions = [
      <FlatButton
        key="cancel"
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        key="logout"
        label="Logout"
        primary={true}
        onTouchTap={this.logout}
      />,
    ]
    
    return (
      <MuiThemeProvider muiTheme={eventioTheme}>
        <div>
          <Dialog
            actions={actions}
            modal={false}
            open={this.state.redirectModalOpen}
            onRequestClose={this.handleClose}
          >
            Your session timed out, would you like to log in again?
          </Dialog>
          {this.props.children}
        </div>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, { logout })(App)