import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import PropTypes from 'prop-types'
import { Card } from 'material-ui/Card'
import { TextField, DatePicker, TimePicker } from 'redux-form-material-ui' 

class EditEventCard extends Component {
  static propTypes = {
    attendeeCount: PropTypes.number.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object
  }

  now = new Date()
  formatter = value => value === '' ? null : (typeof value === 'string') ? new Date(value) : value
  required = value => (value == '' ? 'Required' : undefined)
  date = value => (value < this.now ? 'Must be a later date' : undefined)
  capacity = value => (value < this.props.attendeeCount ? 'Must not be lower than current attendance' : undefined)

  render() {
    return (
      <Card className="edit-event-card">
        <form onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
          <Field name="date"
              floatingLabelText="Date"
              fullWidth={true}
              format={this.formatter}
              component={DatePicker}
              errorText={this.props.errors.date}
              validate={this.date} />
          <Field name="time"
              floatingLabelText="Time"
              fullWidth={true}
              format={this.formatter}
              component={TimePicker} />
          <Field name="title"
              floatingLabelText="Title"
              type="text"
              fullWidth={true}
              component={TextField}
              validate={this.required} />
          <Field name="description"
              floatingLabelText="Description"
              type="text"
              fullWidth={true}
              component={TextField}
              validate={this.required} />
          <Field name="capacity"
              floatingLabelText="Capacity"
              type="number"
              fullWidth={true}
              component={TextField}
              validate={[this.required, this.capacity]} />
        </form>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  let errors = {}
  if (state.form && state.form.editevent) {
    if (state.form.editevent.syncErrors) {
      errors = {...state.form.editevent.syncErrors}
    }
  }
  return {
    errors
  }
}

export default reduxForm({
  form: 'editevent'
})(connect(mapStateToProps, null)(EditEventCard))