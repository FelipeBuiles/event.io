import React, { Component } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'
import { Card, CardTitle, CardText, CardActions } from 'material-ui/Card'
import moment from 'moment'
import RaisedButton from 'material-ui/RaisedButton'
import SvgIcon from 'material-ui/SvgIcon'
import SocialPerson from 'material-ui/svg-icons/social/person'

class EventCard extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    event: PropTypes.object.isRequired,
    edit: PropTypes.func.isRequired,
    leave: PropTypes.func.isRequired,
    join: PropTypes.func
  }

  renderActionButton = () => {
    let { user, event } = this.props
    if (user._id == event.owner._id) {
      return (
        <RaisedButton label={'Edit'}
            backgroundColor="#D9DCE1"
            labelColor="#999EA3"
            className="card-button"
            onClick={() => this.props.edit(event.id)} />
      )
    }
    if (event.attendees.map(u => u._id).includes(user._id)) {
      return (
        <RaisedButton label={'Leave'}
          secondary={true}
          className="card-button"
          onClick={() => this.props.leave(event._id)} />
      )
    }
    return (
      <RaisedButton label={'Join'}
        primary={true}
        className="card-button"
        onClick={() => this.props.join(event._id)} />
    )
  }

  render() {
    const { event } = this.props
    const owner = `${event.owner.firstName} ${event.owner.lastName}`
    return (
      <Card className="event-card">
        <CardText className="event-date">
          {moment(event.startsAt).format('MMMM D, YYYY - h:mm A')}
        </CardText>
        <Link to={`event/${event._id}`}>
          <CardTitle className="event-title" title={event.title} subtitle={owner}/>
        </Link>
        <CardText className="event-description">
          {event.description}
        </CardText>
        <CardActions className="actions">
          <SvgIcon className="person-icon">
            <SocialPerson />
          </SvgIcon>
          <CardText className="attendee-count">{`${event.attendees.length} of ${event.capacity}`}</CardText>
          {this.renderActionButton()}
        </CardActions>
      </Card>
    )
  }
}

export default EventCard