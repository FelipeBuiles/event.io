import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import PropTypes from 'prop-types'
import { Card, CardTitle } from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import { TextField, DatePicker, TimePicker } from 'redux-form-material-ui' 

class CreateEventForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    errors: PropTypes.object
  } 

  now = new Date()
  formatter = value => value === '' ? null : (typeof value === 'string') ? new Date(value) : value
  required = value => ((value == '' || value == null) ? 'Required' : undefined)
  date = value => (value < this.now ? 'Must be a later date' : undefined)
  capacity = value => (value <= 0 ? 'Must not be higher than zero' : undefined)

  render() {
    return (
      <Card className="create-form">
          <CardTitle className="form-title"
            title="Create new event"
            subtitle="Enter details below." />
          <form onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
            <Field name="title"
                floatingLabelText="Title"
                type="text"
                fullWidth={true}
                component={TextField}
                validate={this.required} />
            <Field name="description"
                floatingLabelText="Description"
                type="text"
                fullWidth={true}
                component={TextField}
                validate={this.required} />
            <Field name="date"
                floatingLabelText="Date"
                fullWidth={true}
                format={this.formatter}
                component={DatePicker}
                errorText={this.props.errors.date}
                validate={this.date} />
            <Field name="time"
                floatingLabelText="Time"
                fullWidth={true}
                format={this.formatter}
                component={TimePicker} />
            <Field name="capacity"
                floatingLabelText="Capacity"
                type="number"
                fullWidth={true}
                component={TextField}
                validate={[this.required, this.capacity]} />

            <RaisedButton label="Create new event"
              style={{width: 240, height: 57}}
              type="submit"
              primary={true}/>
          </form>
        </Card>
    )
  }
}

const mapStateToProps = state => {
  let errors = {}
  if (state.form && state.form.createevent) {
    if (state.form.createevent.syncErrors) {
      errors = {...state.form.createevent.syncErrors}
    }
  }
  return {
    errors
  }
}

export default connect(mapStateToProps, null)(reduxForm({
  form: 'createevent'
})(CreateEventForm))