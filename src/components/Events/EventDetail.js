import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Card, CardTitle } from 'material-ui/Card'
import Chip from 'material-ui/Chip'
import Header from '../Common/Header'
import EventCard from './EventCard'
import EditEventCard from './EditEventCard'
import { 
  joinEvent,
  leaveEvent,
  loadEvent,
  updateEvent,
  deleteEvent } from '../../actions/eventActions'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import SvgIcon from 'material-ui/SvgIcon'
import ModeEdit from 'material-ui/svg-icons/editor/mode-edit'
import Done from 'material-ui/svg-icons/action/done'
import Delete from 'material-ui/svg-icons/action/delete'
import './event.scss'
import './eventDetail.scss'

class EventDetail extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  static propTypes = {
    loadEvent: PropTypes.func.isRequired,
    updateEvent: PropTypes.func.isRequired,
    joinEvent: PropTypes.func.isRequired,
    leaveEvent: PropTypes.func.isRequired,
    deleteEvent: PropTypes.func.isRequired,
    event: PropTypes.object,
    user: PropTypes.object,
    ownsEvent: PropTypes.bool,
    location: PropTypes.object,
    params: PropTypes.object
  }

  state = {
    editing: false
  }

  componentWillMount() {
    if (this.props.location.state && this.props.location.state.editing) {
      this.toggleEditMode()
    }
    if (!this.props.event) {
      this.props.loadEvent(this.props.params.id)
    }
  }

  saveChanges = () => {
    this.refs.editEvent.submit()
  }

  joinEvent = id => {
    this.props.joinEvent(id)
  }

  leaveEvent = id => {
    this.props.leaveEvent(id)
  }

  deleteEvent = () => {
    this.props.deleteEvent(this.props.event.id)
    this.context.router.push('/events')
  }

  renderAttendees = () => {
    return this.props.event.attendees.map(a => {
      return (
        <Chip key={a._id}
          style={{margin: 5, display: 'inline-block'}}
          >
          {`${a.firstName} ${a.lastName}`}
        </Chip>
      )
    })
  }

  toggleEditMode = () => {
    this.setState({
      editing: !this.state.editing
    })
  }

  renderActionButton = () => {
    if (this.state.editing) {
      return (
        <FloatingActionButton className="add-event"
          onClick={this.saveChanges.bind(this)}
          backgroundColor={'#22D486'}>
          <Done />
        </FloatingActionButton>
      )
    } else {
      return (
        <FloatingActionButton className="add-event"
          onClick={this.toggleEditMode}
          backgroundColor={'#323C46'}>
          <ModeEdit />
        </FloatingActionButton>
      )
    }
  }

  renderDeleteAction = () => {
    if (this.state.editing && this.props.ownsEvent) {
      return (
        <div className="delete-event"
          onClick={this.deleteEvent}>
          <SvgIcon>
            <Delete />
          </SvgIcon>
          <span className="delete-text">
            Delete event
          </span>
        </div>
      )
    }
  }

  submitEdit = values => {
    let newEventData = {}
    let newDate = moment(values.date).startOf('day')
    let newHour = moment(values.time)
    newDate = newDate
                .add(newHour.get('hour'), 'hours')
                .add(newHour.get('minute'), 'minutes')
                .add(newHour.get('second'), 'seconds')
    let oldDate = moment(this.props.event.startsAt)
    if (!newDate.isSame(oldDate, 'minute')) {
      newEventData.startsAt = newDate
    }
    if (values.title != this.props.event.title) {
      newEventData.title = values.title
    }
    if (values.description != this.props.event.description) {
      newEventData.description = values.description
    }
    if (values.capacity != this.props.event.capacity) {
      newEventData.capacity = values.capacity
    }
    if (newEventData) {
      this.toggleEditMode()
      this.props.updateEvent(this.props.event.id, newEventData)
    }
  }

  render() {
    let eventData = {}
    if (this.props.event) {
      eventData = {
        date: this.props.event.startsAt,
        time: this.props.event.startsAt,
        title: this.props.event.title,
        description: this.props.event.description,
        capacity: this.props.event.capacity
      }
    }
    return (
      <div>
        <Header />
        <div className={`detail-container ${this.state.editing ? 'editing' : ''}`}>
          <div className="detail-head">
            <p className="event-id">Detail event: #{this.props.params.id}</p>
            {this.renderDeleteAction()}
          </div>
          {this.props.event && 
            <div className="detail-inner-container">
              {this.state.editing ? (
                <EditEventCard initialValues={eventData}
                  attendeeCount={this.props.event.attendees.length}
                  onSubmit={this.submitEdit}
                  ref="editEvent"/>
              ) : (
                <EventCard event={this.props.event}
                  user={this.props.user}
                  join={this.joinEvent}
                  leave={this.leaveEvent}
                  edit={this.toggleEditMode}/>
              )}

              <Card className="attendees-card">
                <CardTitle title="Attendees" />
                {this.renderAttendees()}
              </Card>
            </div>
          }
          {this.props.ownsEvent && this.renderActionButton()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let event = state.events.find(e => e.id === ownProps.params.id)
  let user = state.user
  let ownsEvent = false
  if (event) {
    ownsEvent = event.owner.id == state.user.id
  }
  return {
    event,
    user,
    ownsEvent
  }
}

export default connect(mapStateToProps, { 
  loadEvent,
  updateEvent,
  joinEvent,
  leaveEvent,
  deleteEvent
})(EventDetail)