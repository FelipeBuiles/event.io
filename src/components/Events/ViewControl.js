import React from 'react'
import PropTypes from 'prop-types'
import SvgIcon from 'material-ui/SvgIcon'
import ViewModule from 'material-ui/svg-icons/action/view-module'
import ViewStream from 'material-ui/svg-icons/action/view-stream'

const ViewControl = ({currentActive, setActive}) => { 

  return (
    <div className="view-controls">
      <SvgIcon className={currentActive == 'grid' ? 'active' : ''}
        onClick={() => setActive('grid')}>
        <ViewModule />
      </SvgIcon>
      <SvgIcon className={currentActive == 'list' ? 'active' : ''}
        onClick={() => setActive('list')}>
        <ViewStream />
      </SvgIcon>
    </div>
  )
}

ViewControl.propTypes = {
  currentActive: PropTypes.string.isRequired,
  setActive: PropTypes.func.isRequired
}

export default ViewControl