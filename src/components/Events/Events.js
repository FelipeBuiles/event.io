import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { PropTypes } from 'prop-types'
import Header from '../Common/Header'
import EventsList from './EventsList'
import { loadEvents } from '../../actions/eventActions'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import './event.scss'

class Events extends Component {
  static propTypes = {
    events: PropTypes.array,
    user: PropTypes.object.isRequired,
    loadEvents: PropTypes.func.isRequired
  }
  
  componentDidMount() {
    this.props.loadEvents()
  }

  render() {
    return (
      <div>
        <Header />
        <EventsList />
        <Link to="create">
          <FloatingActionButton className="add-event"
            backgroundColor={'#323C46'}>
            <ContentAdd />
          </FloatingActionButton>
        </Link>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    events: state.events,
    user: state.user
  }
}

export default connect(mapStateToProps, { loadEvents })(Events)