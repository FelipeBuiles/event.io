import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import SvgIcon from 'material-ui/SvgIcon'
import ArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down'

export default class FilterControl extends Component {
  static propTypes = {
    currentActive: PropTypes.string.isRequired,
    setActive: PropTypes.func.isRequired
  }

  state = {
    filtermenuOpen: false
  }

  isMobile = () => {
    return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)
  }

  handleOpenMenu = e => {
    e.preventDefault()
    this.setState({
      filtermenuOpen: true,
      anchor: e.currentTarget
    })
  }

  handleCloseMenu = () => {
    this.setState({
      filtermenuOpen: false
    })
  }

  setAll = () => {
    this.props.setActive('all')
    this.handleCloseMenu()
  }

  setPast = () => {
    this.props.setActive('past')
    this.handleCloseMenu()
  }

  setFuture = () => {
    this.props.setActive('future')
    this.handleCloseMenu()
  }

  render() {
    const { currentActive } = this.props
    if (this.isMobile()) {
      return (
        <div className="filter-controls" onClick={this.handleOpenMenu}>
          <span>Show:&nbsp;</span><span className="active">{currentActive}</span>
          <SvgIcon>
            <ArrowDropDown />
          </SvgIcon>
          <Popover
            open={this.state.filtermenuOpen}
            onRequestClose={this.handleCloseMenu}
            anchorEl={this.state.anchor}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'top'}}
            >
            <Menu>
              <MenuItem primaryText="All events" onTouchTap={this.setAll} />
              <MenuItem primaryText="Past events" onTouchTap={this.setPast}/>
              <MenuItem primaryText="Future events" onTouchTap={this.setFuture}/>
            </Menu>
          </Popover>
        </div>
      )
    } else {
      return (
        <div className="filter-controls">
          <span className={currentActive == 'all' ? 'active' : ''}
            onClick={this.setAll}>All events</span>
          <span className={currentActive == 'future' ? 'active' : ''}
            onClick={this.setFuture}>Future events</span>
          <span className={currentActive == 'past' ? 'active' : ''}
            onClick={this.setPast}>Past events</span>
        </div>
      )
    }
  }
}