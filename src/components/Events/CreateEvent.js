import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import SvgIcon from 'material-ui/SvgIcon'
import Close from 'material-ui/svg-icons/navigation/close'
import CreateEventForm from './CreateEventForm'
import { createEvent } from '../../actions/eventActions'
import moment from 'moment'
import './createEvent.scss'

class CreateEvent extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  static propTypes = {
    createEvent: PropTypes.func.isRequired
  }

  submit = event => {
    let newEvent = {...event}
    let date = moment(event.date).startOf('day')
    let hour = moment(event.time)
    newEvent.startsAt = date
                      .add(hour.get('hour'), 'hours')
                      .add(hour.get('minute'), 'minutes')
                      .add(hour.get('second'), 'seconds')
                      .toDate()
    delete newEvent.date
    delete newEvent.time
    this.props.createEvent(newEvent)
    this.context.router.push('/events')
  }

  render() {
    return (
      <div className="create-event">
        <div className="head">
          <img className="logo"
            alt="Eventio logo"
            src={require('../../assets/images/Logo.png')}
            />
          <Link to="/events" className="close-create">
            <SvgIcon>
              <Close />
            </SvgIcon>
            <span className="close-text">Close</span>
          </Link>
        </div>
        <CreateEventForm onSubmit={this.submit} />
      </div>
    )
  }
}

export default connect(null, { createEvent })(CreateEvent)