import React, { Component } from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'
import Paper from 'material-ui/Paper'
import moment from 'moment'
import RaisedButton from 'material-ui/RaisedButton'

class EventTile extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    event: PropTypes.object.isRequired,
    leave: PropTypes.func.isRequired,
    join: PropTypes.func,
    edit: PropTypes.func
  }

  renderActionButton = () => {
    let { user, event } = this.props
    if (user._id == event.owner._id) {
      return (
        <RaisedButton label={'Edit'}
            backgroundColor="#D9DCE1"
            labelColor="#999EA3"
            className="card-button"
            onClick={() => this.props.edit(event.id)} />
      )
    }
    if (event.attendees.map(u => u._id).includes(user._id)) {
      return (
        <RaisedButton label={'Leave'}
          secondary={true}
          className="card-button"
          onClick={() => this.props.leave(event._id)} />
      )
    }
    return (
      <RaisedButton label={'Join'}
        primary={true}
        className="card-button"
        onClick={() => this.props.join(event._id)} />
    )
  }

  render() {
    const { event } = this.props
    const owner = `${event.owner.firstName} ${event.owner.lastName}`
    return (
      <Paper className="event-tile">
        <Link to={`event/${event._id}`}>
          <span className="event-title">
            {event.title} 
          </span>
        </Link>
        <span className="event-description">
          {event.description}
        </span>
        <span className="event-owner">
          {owner}
        </span>
        <span className="event-date">
          {moment(event.startsAt).format('MMMM D, YYYY - h:mm A')}
        </span>
        <span className="attendee-count">
          {`${event.attendees.length} of ${event.capacity}`}
        </span>
        {this.renderActionButton()}
      </Paper>
    )
  }
}

export default EventTile