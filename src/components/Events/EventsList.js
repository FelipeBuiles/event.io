import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import EventCard from './EventCard'
import EventTile from './EventTile'
import FilterControl from './FilterControl'
import ViewControl from './ViewControl'
import { joinEvent, leaveEvent } from '../../actions/eventActions'

class EventsList extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  static propTypes = {
    events: PropTypes.array,
    user: PropTypes.object.isRequired,
    join: PropTypes.func.isRequired,
    leave: PropTypes.func.isRequired
  }

  state = {
    filterType: 'all',
    displayMode: 'grid'
  }

  setFilterType = type => {
    this.setState({
      filterType: type
    })
  }

  setViewType = type => {
    this.setState({
      displayMode: type
    })
  }

  joinEvent = id => {
    this.props.join(id)
  }

  leaveEvent = id => {
    this.props.leave(id)
  }

  editEvent = id => {
    this.context.router.push({
      pathname: `/event/${id}`,
      state: {editing: true}
    })
  }

  renderEvents = () => {
    let filteredEvents = []
    let now = new Date()
    switch (this.state.filterType) {
      case 'future':
        filteredEvents = this.props.events.filter(e => new Date(e.startsAt) > now)
        break

      case 'past':
        filteredEvents = this.props.events.filter(e => new Date(e.startsAt) < now)
        break

      case 'all':
      default:
        filteredEvents = this.props.events
    }
    return filteredEvents
      .sort()
      .map(e => {
        if (this.state.displayMode == 'grid') {
          return (
            <EventCard key={e._id} 
              event={e}
              edit={this.editEvent}
              user={this.props.user}
              join={this.joinEvent}
              leave={this.leaveEvent}/>
          )
        } else {
          return (
            <EventTile key={e._id}
              event={e}
              edit={this.editEvent}
              user={this.props.user}
              join={this.joinEvent}
              leave={this.leaveEvent} />
          )
        }
      })
  }

  render() {
    return (
      <div className="list-container">
        <div className="top-controls">
          <FilterControl currentActive={this.state.filterType}
            setActive={this.setFilterType} />
          <ViewControl currentActive={this.state.displayMode}
            setActive={this.setViewType} />
        </div>
        <div className={`list-area ${this.state.displayMode === 'list' ? 'list-mode' : ''}`}>
          {this.renderEvents()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    events: state.events,
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    join: id => {
      dispatch(joinEvent(id))
    },
    leave: id => {
      dispatch(leaveEvent(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventsList)