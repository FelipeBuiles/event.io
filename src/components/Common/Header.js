import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import Avatar from 'react-avatar'
import PropTypes from 'prop-types'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import SvgIcon from 'material-ui/SvgIcon'
import ArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down'
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back'

import { logout } from '../../actions/userActions'
import './header.scss'

class Header extends Component {
  static contextTypes = {
    router: PropTypes.object
  }

  static propTypes = {
    user: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired
  }

  state = {
    usermenuOpen: false
  }

  handleOpenMenu = e => {
    e.preventDefault()
    this.setState({
      usermenuOpen: true,
      anchor: e.currentTarget
    })
  }

  handleCloseMenu = () => {
    this.setState({
      usermenuOpen: false
    })
  }

  handleLogout = () => {
    this.props.logout()
  }

  render() {
    const username = `${this.props.user.firstName} ${this.props.user.lastName}`
    return (
      <div className="header">
        <Link to="/events">
          <img className="logo"
            alt="Eventio logo"
            src={require('../../assets/images/Logo.png')}
            />
        </Link>
        {(this.context.router.location.pathname.includes('event/')) &&
          <Link to="/events" className="back-link">
            <SvgIcon>
              <ArrowBack />
            </SvgIcon>
            <span>Back to events</span>
          </Link>
        } 
        <div className="user-menu"
          onClick={this.handleOpenMenu}
          >
          <Avatar name={username} size={40} className="avatar"/>
          <span className="user-name">{username}</span>
          <SvgIcon>
            <ArrowDropDown />
          </SvgIcon>
        </div>
        <Popover
          open={this.state.usermenuOpen}
          onRequestClose={this.handleCloseMenu}
          anchorEl={this.state.anchor}
          anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          >
          <Menu>
            <MenuItem primaryText="Profile" containerElement={<Link to="/profile" />} />
            <MenuItem primaryText="Logout" onTouchTap={this.handleLogout}/>
          </Menu>
        </Popover>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, { logout })(Header)