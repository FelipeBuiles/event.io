import React from 'react'
import './panel.scss'

const SidePanel = () => {
  return (
    <div className="left-panel">
      <img className="logo"
        alt="Eventio logo"
        src={require('../../assets/images/Logo-white.png')}
        />
      <q className="quote">
        Great, kid. Don’t get cocky.
      </q>
      <div className="separator" />
      <p className="han">Han Solo</p>
    </div>
  )
}

export default SidePanel