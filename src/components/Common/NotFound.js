import React from 'react'
import { Link } from 'react-router' 
import SidePanel from './SidePanel'
import RaisedButton from 'material-ui/RaisedButton'
import './notfound.scss'

const btnStyle = {
  width: 240,
  height: 57
}

export const NotFound = () => {
  return (
    <div className="notfound-container">
      <SidePanel />
      <div className="main-panel">
        <img className="trooper"
          alt="Stormtrooper background"
          src={require('../../assets/images/trooper.png')}
          />
        <div className="center-panel">
          <h1>Something went wrong.</h1>
          <p>Seems like Darth Vader hit our website and knocked it down.</p>
          <p>Hit refresh and everything should be fine again.</p>
          <Link to="/" className="redirect">
            <RaisedButton style={btnStyle} 
            backgroundColor={'#323C46'}
            labelColor={'#FFF'}
            label="Refresh"/>
          </Link>
        </div>
      </div>
    </div>
  )
}