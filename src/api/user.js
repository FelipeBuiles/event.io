import { post } from './common'

export function login({email, password}) {
  window.sessionStorage.removeItem('authorization')
  return post('auth/native', {
    email,
    password
  })
}

export function signup(firstName, lastName, email, password) {
  return post('users', {
    firstName,
    lastName,
    email,
    password
  })
}