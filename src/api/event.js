import { get, post, patch, del } from './common'

export function list() {
  return get('events')
}

export function getSpecific(id) {
  return get(`events/${id}`)
}

export function create(event) {
  return post('events', event)
}

export function update(id, event) {
  return patch(`events/${id}`, event)
}

export function remove(id) {
  return del(`events/${id}`)
}

export function attend(id) {
  return post(`events/${id}/attendees/me`)
}

export function unattend(id) {
  return del(`events/${id}/attendees/me`)
}