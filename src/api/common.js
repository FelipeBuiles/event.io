import * as api from '../constants/api' 

let baseHeaders = new Headers()
baseHeaders.append('APIKey', api.API_KEY)

function addAuth(headers) {
  let newHeaders = cloneHeader(headers)
  let token = window.sessionStorage.getItem('authorization')
  if (token) {
    newHeaders.append('authorization', token)
  }
  return newHeaders
}

function addContentType(headers) {
  let newHeaders = cloneHeader(headers)
  newHeaders.append('Content-Type', 'application/json')
  return newHeaders
}

function cloneHeader(h) {
  let clonedHeader = new Headers()
  for (let entry of h.entries()) {
    clonedHeader.append(entry[0], entry[1])
  }
  return clonedHeader
}

const compose = (f1, f2) => value => f1(f2(value))
const setupHeaders = compose(addAuth, addContentType)

export function get(endpoint) {
  const headers = setupHeaders(baseHeaders)
  return fetch(api.API_URL + endpoint, {
    method: 'GET',
    headers
  })
}

export function post(endpoint, data) {
  const headers = setupHeaders(baseHeaders)  
  return fetch(api.API_URL + endpoint, {
    method: 'POST',
    headers,
    body: JSON.stringify(data)
  })
}

export function patch(endpoint, data) {
  const headers = setupHeaders(baseHeaders)
  return fetch(api.API_URL + endpoint, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(data)
  })
}

export function del(endpoint) {
  const headers = setupHeaders(baseHeaders)
  return fetch(api.API_URL + endpoint, {
    method: 'DELETE',
    headers
  })
}