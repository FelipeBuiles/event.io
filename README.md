# Eventio

## https://eventio.surge.sh/

A test project for STRV using React and Redux plus a whole lot of different web technologies and libraries, Eventio lets you see, sign up for and attend events, also create them and share with friends.

[![Coverage Status](https://coveralls.io/repos/bitbucket/FelipeBuiles/event.io/badge.svg?branch=master&t=PfbCIA)](https://coveralls.io/bitbucket/FelipeBuiles/event.io?branch=master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

+ node version >= 6
+ preferably yarn, npm is also fine


### Installing

A step by step series of examples that tell you have to get a development env running

Install dependencies
```
yarn install
```

Run local development server
(this step also lints the code, runs tests, and opens up a browser tab with the running app)
```
yarn start
```

Whenever you're ready to generate a production ready app
(this step minifies and bundles up everything and creates a `dist/` folder ready to deploy)
```
yarn run build
```

## Running the tests

To create a new test just make a new file with the suffix `.spec.js`, the jest server will pick it up automatically

To run the tests, run the command
```
yarn test
```

### And coding style tests

To run the linter on its own you just run its task like this
```
yarn run lint
```
or you could make it watch for changes with
```
yarn run lint:watch
```

## Built With

* [React](https://facebook.github.io/react/)
* [Redux](http://redux.js.org/)
* [React Router](https://reacttraining.com/react-router/)
* [Material UI](http://www.material-ui.com/)
* [Webpack](https://webpack.js.org/)
* [Babel](https://babeljs.io/)
* [Jest](https://facebook.github.io/jest/)

## Acknowledgments

* [Cory House](https://github.com/coryhouse/) - for his amazing tutorials at pluralsight
* [Stack Overflow](https://stackoverflow.com/) - for their invaluable help in time of need
* [STRV](https://www.strv.com/) - for their inspiration